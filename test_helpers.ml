let read_file_by_path (filepath : string) : string =
  let file_in = open_in filepath
  and content = ref ""
  in
  try
    while true do
      let chr = input_char file_in
      in content := !content ^ (String.make 1 chr);
    done;

    close_in file_in; (* unreachable *)
    !content (* unreachable *)
  with
    | End_of_file ->
      close_in file_in;
      !content
    | e ->
        close_in_noerr file_in;
        raise e


and with_temp_file (f : string * out_channel -> unit) : unit =
  let tmpfilename, tmpfile_out = Filename.open_temp_file "_" "_"
  in
  try
    f (tmpfilename, tmpfile_out)
  with e ->
    close_out tmpfile_out;
    raise e;;

