## Installation

Make sure you have the `batteries` and `ounit2` libs installed.
```
$ opam install batteries ounit2
```

`cd` into the project's root folder and build the app for testing first:
```
$ dune build test.exe
```

Run the executable and make sure all tests pass:
```
$ ./_build/default/test.exe
```

Build the app:
```
$ dune build camelserver.exe
```

Run the executable:
```
$ ./_build/default/camelserver.exe "hello\nworld!"
```
