type ctx_t = { trailing_newline: bool; interpret_escapes: bool };;


let rec echo (display : out_channel) (argv : string array) =
  let ctx, args = parse_opts argv

  in
  let args = if ctx.interpret_escapes
             then Array.map unescape args else args

  in
  args |> Array.to_list |> String.concat " " |> output_string display;
  if ctx.trailing_newline then output_newline display;

  flush display


and parse_opts (argv : string array) : ctx_t * string array =
  let split_opts_and_args () =
    let opts =
      argv
      |> Array.to_list
      |> BatList.take_while (
           let filepath = ref true
           in
           (fun s -> if !filepath then
                       begin filepath := false; true end
                     else BatString.starts_with s "-")
         )
      |> Array.of_list

    in
    let args = BatArray.tail argv (Array.length opts)
    in opts, args


  and ctx_of_opts opts =
    let trailing_newline = ref true
    and interpret_escapes = ref false

    in
    let speclist = [
      "-n",
      Arg.Clear trailing_newline,
      "do not output the trailing newline"
      ;

      "-e",
      Arg.Set interpret_escapes,
      "enable interpretation of backslash escapes"
    ]

    and anon_fun _arg = ()
    and usage_msg = ""

    in
    Arg.parse_argv ~current:(ref 0) opts speclist anon_fun usage_msg;

    { trailing_newline = !trailing_newline;
      interpret_escapes = !interpret_escapes }


  in
  let opts, args = split_opts_and_args ()
  in ctx_of_opts opts, args

  
and output_newline (display : out_channel) =
  output_string display "\n"


and unescape (s : string) : string =
  let repl_tbl = [
    ("\\\\", "\\");
    ("\\n", "\n");
    ("\\t", "\t");
  ]

  in
  repl_tbl
  |> List.fold_left (fun str (sub, by) ->
      BatString.nreplace ~str ~sub ~by) s;;


echo stdout Sys.argv;;

