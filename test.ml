open OUnit2;;

let suite = test_list [
  Test_camelecho.echo_tests;
  Test_camelecho.parse_opts_tests;
];;

let () = run_test_tt_main suite;;

