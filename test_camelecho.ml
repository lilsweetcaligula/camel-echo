open OUnit2;;
open Test_helpers;;


let argv (args : string array) : string array =
  Array.concat [[|"/foo/bar/baz/me"|]; args];;


let string_of_string_array (ss : string array) : string =
  Array.to_list ss |> String.concat "; " |> Printf.sprintf "[|%s|]";;


let echo_tests = 
  "echo" >::: [
    "when given no arguments, outputs a new line" >:: (fun _ ->
      with_temp_file (fun (tmpfilename, tmpfile_out) ->
        Camelecho.echo tmpfile_out (argv [||]);
        flush tmpfile_out;

        let actual_output = read_file_by_path tmpfilename
        in assert_equal "\n" actual_output ~printer:Fun.id
      )
    );

    "when given one argument, outputs it followed by a new line" >:: (fun _ ->
      with_temp_file (fun (tmpfilename, tmpfile_out) ->
        Camelecho.echo tmpfile_out (argv [|"silva"|]);
        flush tmpfile_out;

        let actual_output = read_file_by_path tmpfilename
        in assert_equal "silva\n" actual_output ~printer:Fun.id
      )
    );

    "when given multiple arguments, outputs them followed by a new line" >:: (fun _ ->
      with_temp_file (fun (tmpfilename, tmpfile_out) ->
        Camelecho.echo tmpfile_out (argv [|"nauta"; "aquam"; "portat"|]);
        flush tmpfile_out;

        let actual_output = read_file_by_path tmpfilename
        in assert_equal "nauta aquam portat\n" actual_output ~printer:Fun.id
      )
    )
  ]


let parse_opts_tests =
  let string_of_ctx (ctx : Camelecho.ctx_t) : string =
    let { Camelecho.trailing_newline;
          Camelecho.interpret_escapes } = ctx

    in Printf.sprintf "{ trailing_newline = %b; interpret_escapes = %b }\n"
                      trailing_newline interpret_escapes

  in
  "building app context from the user-given options" >::: [
    "when given no options, returns the default ctx" >:: (fun _ ->
      let expected_ctx = { Camelecho.trailing_newline = true;
                           Camelecho.interpret_escapes = false }

      and actual_ctx, _ = Camelecho.parse_opts (argv [||])
      in assert_equal expected_ctx actual_ctx ~printer:string_of_ctx
    );

    "when passed the -n option, indicates that no newline should be printed" >:: (fun _ ->
      let expected_ctx = { Camelecho.trailing_newline = false;
                           Camelecho.interpret_escapes = false }

      and actual_ctx, _ = Camelecho.parse_opts (argv [|"-n"|])
      in assert_equal expected_ctx actual_ctx ~printer:string_of_ctx
    );

    "when passed any options, returns the values to be printed" >:: (fun _ ->
      let expected_values = [|"nauta"; "aquam"; "portat"|]
      and opts = [|"-n"|]
      
      in
      let args = argv (Array.concat [opts; expected_values]) in
      let _, actual_values = Camelecho.parse_opts args

      in
      let values vals =
        let result = Array.copy vals
        in Array.sort String.compare result; result

      in
      assert_equal (values expected_values)
                   (values actual_values) 
                   ~printer:string_of_string_array
    );

    "treats options mixed with values as values" >:: (fun _ ->
      let expected_values = [|"nauta"; "-n"; "aquam"; "portat"|] in
      let _, actual_values = Camelecho.parse_opts (argv expected_values)

      in
      assert_equal expected_values
                   actual_values
                   ~printer:string_of_string_array
    )
  ];;

